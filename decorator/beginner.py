"""
Decorator: 
    Special function which is used to add functionality to the normal function without
    changing any code of original function.
    
    This function takes function as input and return decorated function
    original_function (i/p) -> decorator_function -> decorated_function (o/p)
"""

# I have a function five which return 5
def five():
    return 5

# I want to change add 10 in the function five without changing any code of function five
# For that decorator is used
def add_ten(func):
    def wrapper():
        return func() + 10
    return wrapper
"""
func: original function
add_ten: decorator_function (function that add features to original function)
wrapper: decorated_function
"""

# decorator is just a syntactic sugar of:
# original_function = decorator_function(original_function)
five = add_ten(five)
print("Decorator working", five())

# Since above code is not cool and we haved used decorator syntax till now
# Let's use decorator syntax
def add_ten(func):
    def wrapper():
        return func() + 10
    return wrapper

@add_ten # this is equivalent to new_five = add_ten(new_five) done internally by python
def new_five(): return 5

print("Using @decorator", new_five())
