"""
I have used decorator with and without parameter.
Now I have issue I am not able to see docstring of original function
"""
from functools import wraps

def add_ten(func):
    def wrapper():
        return func() + 10
    return wrapper

@add_ten
def five(): 
    """Return 5"""
    return 5

print("Result", five.__doc__)

# wraps decoration from functools need to be used for docstring of original function
from functools import wraps

def add_five(func):
    @wraps(func)
    def wrapper():
        return func() + 5
    return wrapper

@add_five
def six(): 
    """Return 6"""
    return 6

print("Result", six.__doc__)
