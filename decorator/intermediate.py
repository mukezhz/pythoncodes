"""
Decorator with a parameter:
@decorator(num=8)
I am going to create a decorator function which takes num parameter 
which is used to add dynamic num with original value 
"""

# since @decorator function is just a syntactic sugar of
# original_function = decorator(original_function)
# function is passed automatically by python itself so
"""
def add_num(func, num):
    def wrapper():
        return  func() + num
@add_num(8)
def five(): return 5
"""
# the above will throw error because this is not it is
# actually when using @add_num(8) 
# add_num(8) is only passed to the decorator function
# and the wrapper function is responsible for handling original function
# syntax becomes: decorator(parameter)(func)

def add_num(num):
    def handle_func(func):
        def wrapper():
            return  func() + num
        return wrapper
    return handle_func

@add_num(8)
def five(): return 5

print(five())

# handling multiple parameter in decorator
def decorator(num, *args):
    print("Above handle_func", args)
    result = sum(args)
    def handle_func(func):
        print("Above wrapper", func.__name__)
        def wrapper():
            return  func() + result
        return wrapper
    return handle_func

@decorator(10,1,2,3,4,5)
def ten(): return 10

print(ten())