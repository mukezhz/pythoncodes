# 100 Days of Code

## I am using python for 100 days code challenge.

### My Workstation Spectification:

```
                     ./o.                  willsir@EndeavourOS
                   ./sssso-                -------------------
                 `:osssssss+-              OS: EndeavourOS Linux x86_64
               `:+sssssssssso/.            Host: Inspiron 5570
             `-/ossssssssssssso/.          Kernel: 5.15.16-1-lts
           `-/+sssssssssssssssso+:`        Uptime: 2 hours, 8 mins
         `-:/+sssssssssssssssssso+/.       Packages: 1113 (pacman)
       `.://osssssssssssssssssssso++-      Shell: fish 3.3.1
      .://+ssssssssssssssssssssssso++:     Resolution: 1366x768
    .:///ossssssssssssssssssssssssso++:    DE: Plasma 5.23.5
  `:////ssssssssssssssssssssssssssso+++.   WM: KWin
`-////+ssssssssssssssssssssssssssso++++-   Theme: Breeze Light [Plasma], Breeze
 `..-+oosssssssssssssssssssssssso+++++/`   Icons: [Plasma], candy-icons [GTK2/3
   ./++++++++++++++++++++++++++++++/:.     Terminal: alacritty
  `:::::::::::::::::::::::::------``       CPU: Intel i7-8550U (8) @ 4.000GHz
                                           GPU: Intel UHD Graphics 620
                                           GPU: AMD ATI Radeon R7 M260/M265 / M
                                           Memory: 1911MiB / 15899MiB
```

### Packages using:
1. mariadb: `sudo pacman -S mariadb`
2. pyenv: [Pyenv website](https://github.com/pyenv/pyenv-installer)
3. poetry: `sudo pip install poetry`
4. ipython: `sudo pip install ipython`
5. postgresql: `sudo pacman -S postgresql`

### TO FIX ISSUE OF MARIADB
```
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
```

### TO FIX ISSUE OF POSTGRESQL
```
sudo -iu postgres
initdb -D /var/lib/postgres/data
OR
initdb --locale=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
```
