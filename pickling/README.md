# PICKLE MODULE

**Serialization or Pickling:** Pickling or Serialization is the process of converting a Python object (lists, dict, tuples, etc.) into byte streams that can be saved to disks or can be transferred over a network.

**De-serialization or un pickling:** The byte streams saved on file contains the necessary information to reconstruct the original python object. The process of converting byte streams back to python objects is called de-serialization.


## Pickle is a module in Python for serializing and deserializing. 
- It is the faster and simpler choice for this purpose if we do not need any human-readable format. To use this we should first import using the following command.
`import pickle`

Protocol 
Version    Description
0	Original, human-readable format protocol. It is backward-compatible with earlier versions of Python.
1	Old and binary format. It is also compatible with earlier versions of Python.
2	It is added in Python 2.3 version. It provides efficient pickling of new-style classes.
3	It is introduced in Python 3.0 and works with 3.x versions.  It supports byte objects 
4	Introduced in Python 3.4 version. It supports very large objects, more types of objects. It also contains data format optimizations.

This module provides the following four methods:

-   dump(): This method is used to serialize to an open file object

    `pickle.dump(obj, file, protocol = None, *, fix_imports = True)`
-   dumps(): This method is used for serializing to a string

    `pickle.dumps(obj, protocol = None, *, fix_imports = True)`
-   load(): This method deserializes from an open-like object.

    `pickle.load(file, *, fix_imports = True, encoding = “ASCII”, errors = “strict”)`
-   loads(): This does deserialization from a string.

    `pickle.loads(bytes_object, *, fix_imports = True, encoding = “ASCII”, errors = “strict”)`

## Exceptions in Python Pickle

### There are three important exceptions in Pickle that we need to know and these are:
1. exception pickle.PickleError
2. exception pickle.PicklingError
3. exception pickle.UnpicklingError

