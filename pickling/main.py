import pickle

def serialize():
    nepal = {
            'name': 'Nepal', 'language': 'Nepali',
            }
    india = {
            'name': 'India', 'language': 'Hindi',
            }
    db = {}
    db['nepal'] = nepal
    db['india'] = india

    with open('pickletest.pkl', 'wb') as pklfile:
        pickle.dump(db, pklfile)


def deserialize():
    with open('pickletest.pkl', 'rb') as pklfile:
        db = pickle.load(pklfile)
        for k, v in db.items():
            print(f"{k}=>{v}")

if __name__ == '__main__':
    serialize()
    deserialize()
