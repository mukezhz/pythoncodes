#include <iostream>
class Hello
{
public:
    void hellofunc()
    {
        std::cout << "Hello World!!!" << std::endl;
    }
};
int main()
{
    Hello h;
    h.hellofunc();
    return 0;
}

extern "C"
{
    Hello* getHello() { return new Hello(); }
    void hello(Hello *obj) { obj->hellofunc(); }
}