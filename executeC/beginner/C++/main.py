from ctypes import cdll

lib = cdll.LoadLibrary('./libgeek.so')

class Hello(object):
	def __init__(self):
		self.obj = lib.getHello()

	def hello(self):
		lib.hello(self.obj)

h = Hello()

h.hello()
