#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int checkPrime(int num)
{
    short flag = 0;
    int stop = (int)sqrt(abs(num))+1;
    for (size_t i = 2; i < stop; i++)
    {
        if (num % i == 0)
        {
            flag = 1;
            break;
        }
    }
    return (flag == 0) ? 0 : -1;
}
