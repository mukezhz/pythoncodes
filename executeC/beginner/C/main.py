import ctypes

prime = ctypes.CDLL("./libprime.so")
prime.checkPrime.argtypes = [ctypes.c_int]


num = int(input("Enter the number: "))
if prime.checkPrime(num) == 0:
    print(f"Number {num} is Prime")
else:
    print(f"Number {num} is Not Prime")
