# Calling C  from Python

### Benifits of this:
1. C has faster execution speed and to overcome the limitation of Global Interpreter Lock(GIL) in python, python bindings are helpful.
2. We have a large, stable and tested library in C/C++, which will be advantageous to use.
3. For performing large scale testing of the systems using Python test tools.
4. Python code is user friendly.

### Compile this code to the shared library 

```
gcc -fPIC -shared -o <shared library name>.so <source code>.c
```

# Calling C++  from Python

### Note: Since we cannot directly call C++ code using python we need to provide those cpp declarations as extern “C” because ctypes can only interact with C functions.
```
extern "C" {
	<Class>* <Function name>(){ return new <Class>(); }
	void <Function name>(<Class>* <obj>){ <obj> -> <method name>(); }
}

```
### To compile into shared library
```
gcc -c -fPIC <src file>.cpp -o <obj file>.o

g++ -shared -Wl,-soname,<library name>.so -o <library name>.so <obj file>.o
```
