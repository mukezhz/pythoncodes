import sqlite3


class SQLiteManager:
    def __init__(self, dbname):
        self.dbname = dbname
        self.connection = None

    def __enter__(self):
        self.connection = sqlite3.connect(self.dbname)
        return self.connection

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.connection.close()
