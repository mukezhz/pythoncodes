import sqlite3
from sqlitemanager import SQLiteManager


def create_table(sqlite):
    print("Opened database successfully for creating")
    try:
        sqlite.execute(
            """CREATE TABLE COMPANY
         (ID INT PRIMARY KEY     NOT NULL,
         NAME           TEXT    NOT NULL,
         AGE            INT     NOT NULL,
         ADDRESS        CHAR(50),
         SALARY         REAL);"""
        )
    except sqlite3.OperationalError:
        print("Table already exists")
    else:
        print("Table created successfully")


def insert_into(sqlite):
    print("Opened database successfully for inserting")
    try:
        sqlite.execute(
            "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
        VALUES (1, 'Paul', 32, 'California', 20000.00 )"
        )
        sqlite.execute(
            "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
        VALUES (2, 'Allen', 25, 'Texas', 15000.00 )"
        )
        sqlite.execute(
            "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
        VALUES (3, 'Teddy', 23, 'Norway', 20000.00 )"
        )
        sqlite.execute(
            "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
        VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 )"
        )
    except sqlite3.IntegrityError:
        print("UNIQUE constraint failed")
    else:
        print("Records created successfully")
    sqlite.commit()


def select_operation(sqlite):
    print("Opened database successfully for SELECT")

    cursor = sqlite.execute("SELECT id, name, address, salary from COMPANY")
    for row in cursor:
        print("ID = ", row[0])
        print("NAME = ", row[1])
        print("ADDRESS = ", row[2])
        print("SALARY = ", row[3], "\n")

    print("Operation done successfully")


def update_operation(sqlite):
    print("Opened database successfully for UPDATE")

    conn.execute("UPDATE COMPANY set SALARY = 25000.00 where ID = 1")
    conn.commit()
    print("Total number of rows updated :", conn.total_changes)

    cursor = sqlite.execute("SELECT id, name, address, salary from COMPANY")
    for row in cursor:
        print("ID = ", row[0])
        print("NAME = ", row[1])
        print("ADDRESS = ", row[2])
        print("SALARY = ", row[3], "\n")

    print("Operation done successfully")


def delete_operation(sqlite):
    print("Opened database successfully for delete")

    sqlite.execute("DELETE from COMPANY where ID = 2;")
    sqlite.commit()
    print("Total number of rows deleted :", sqlite.total_changes)

    cursor = sqlite.execute("SELECT id, name, address, salary from COMPANY")
    for row in cursor:
        print("ID = ", row[0])
        print("NAME = ", row[1])
        print("ADDRESS = ", row[2])
        print("SALARY = ", row[3], "\n")

    print("Operation done successfully")


with SQLiteManager("company.db") as sqlite:
    create_table(sqlite)
    insert_into(sqlite)
    select_operation(sqlite)
