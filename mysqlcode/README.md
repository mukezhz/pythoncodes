# Mysql connection using API
- pip install mysqlclient
- poetry add mysqlclient

## [Official docs of mysqlclient](https://mysqlclient.readthedocs.io)

### Why to use cursor when dealing with SQL?
### Cursor
1. The database cursor is a data structure that helps one iterate through the result in an ordered manner. 
2. It helps abstract the concepts of ordering in the underlying data.
3. It helps to perform row by row operations. 
4. Cursors need not hit the database for each iteration. The underlying DBMS can send data in blocks to reduce the number of round trips one has to make between the client and the database server.
5. The result set of the cursor is temporarily stored which means there is no need to recompute the query result with each cursor fetch.
6. For example:

```
What happens if you run a query that returns 100 GB of data in a billion records?

That would fill up the RAM on both computers and probably crash the client.

The cursor allows the server to send out the data in smaller batches which also helps the client by not overwhelming it with data.
```

### Steps to use mysql api

1. import the mysql_drive using ``import MySQLdb as mysql``
2. get the connection object: ``conn = mysql.connect(host, user, passwd, db)``
3. get the cursor object from connection object: ``cur = conn.cursor()``
4. execute the query using cursor object: ``cur.execute(<query>)``
5. close the cursor: ``cur.close()``
6. close the connection: ``conn.close()``

### Add your credentials to demo.env : Check demo_sample.env for sample
```
export variable to shell:
for fish:
source (sed -rn 's/^([^=]+)=(.*)/set -x \1 \2/p' demo.env | psub )

for bash/zsh:
export $(grep -v '^#' demo.env | xargs -0)
```