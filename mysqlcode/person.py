from os import environ
import MySQLdb as mysql
from prettytable import PrettyTable

from mysqlmanager import MySQLManager
from utils import create_table, insert_into, select_operation

hostname = environ.get("HOST")
username = environ.get("USER")
password = environ.get("PASSWD")
database = environ.get("DB")


with MySQLManager(hostname, username, password, database) as sql:
    conn = sql
    cur = conn.cursor()
    print("Obtained cursor")
    create_table(cur, "Persons")
    insert_into(conn, cur)
    if select_operation(cur, "Persons") > 0:
        results = cur.fetchall()
        p = PrettyTable(["id", "first name", "last name", "address", "city"])
        for data in results:
            id, l_name, f_name, address, city = data
            p.add_row([id, f_name, l_name, address, city])
        print(p)
    cur.close()
