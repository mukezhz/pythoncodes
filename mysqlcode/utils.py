from MySQLdb import OperationalError, IntegrityError


def create_table(cur, table_name):
    print("Start table creation")
    q = f"""
        CREATE TABLE {table_name} (
        PersonID int NOT NULL,
        LastName varchar(255),
        FirstName varchar(255),
        Address varchar(255),
        City varchar(255),
        PRIMARY KEY (PersonID)
    );
    """
    try:
        cur.execute(q)
    except OperationalError:
        print("Table already exists")
    else:
        print("Table create successfully")


def insert_into(conn, cur):
    print("Start insertion")
    q = """
        INSERT INTO Persons (PersonID, LastName, FirstName, Address, City)
        VALUES (2, 'Poudel', 'Kiran', 'Balkumari', 'Lalitpur');
    """
    try:
        cur.execute(q)
        conn.commit()
    except IntegrityError:
        print("Trying to insert Duplicate value maybe same key")
    else:
        print("Insertion sucessful")

def select_operation(cur, table_name):
    print("Select operation starts")
    q = f"""
        SELECT * FROM {table_name} ;
    """
    try:
        return cur.execute(q)
    except OperationalError:
        print(f"No table exists with table name {table_name}")
    else:
        print("Select operation ends")